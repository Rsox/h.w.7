<?php
$money = [
        'uah' => [
            'name' => 'Гривна',
            'course' => '1'
        ],
        'usd' => [
            'name' => 'Доллар',
            'course' => '27.1'
        ],
        'eur' => [
            'name' => 'Евро',
            'course' => '30.2'
        ],
    ];
    $goods = [
        [
            'title' => 'Носки',
            'price_val' => 125,
            'discount_type' => 'percent',
            'discount_val' => 5
        ],
        [
            'title' => 'Штаны',
            'price_val' => 233,
            'discount_type' => 'percent',
            'discount_val' => 3
        ],
        [
            'title' => 'Трусы',
            'price_val' => 215,
            'discount_type' => 'percent',
            'discount_val' => 2
        ],
        [
            'title' => 'Кепка',
            'price_val' => 330,
            'discount_type' => 'percent',
            'discount_val' => 5
        ],
        [
            'title' => 'Футболка',
            'price_val' => 435,
            'discount_type' => 'percent',
            'discount_val' => 10
        ],
        [
            'title' => 'Свитер',
            'price_val' => 140,
            'discount_type' => 'value',
            'discount_val' => 30
        ],
        [
            'title' => 'Спортивный костюм',
            'price_val' => 680,
            'discount_type' => 'value',
            'discount_val' => 60
        ],
        [
            'title' => 'Куртка',
            'price_val' => 1150,
            'discount_type' => 'value',
            'discount_val' => 130
        ],
        [
            'title' => 'Кеды',
            'price_val' => 1290,
            'discount_type' => 'value',
            'discount_val' => 120
        ],
        [
            'title' => 'Рейтузы',
            'price_val' => 417,
            'discount_type' => 'value',
            'discount_val' => 42
        ]
    ]
?>
