<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    session_start();
        require_once ('data/data.php');
        require_once ('functions/functions.php');
    //Проверяем сессию, если её нету, то присваиваем ей гривну по умолчанию
    if (!isset($_POST['viewCur'])){
        $_SESSION['viewCur'] = 'uah';
    } else {
        $_SESSION['viewCur'] = htmlspecialchars($_POST['viewCur']);
    }
    $course = $_SESSION['viewCur'];
    var_dump($_SESSION['viewCur']);


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        body {
            background-color: lightblue;
        }
    </style>
    <title>Magazin</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <form class="was-validated" method="post" action="index.php">
                <div class="form-group" >
                    <select class="custom-select" name="viewCur">
                    	<!-- Перебираем массив с валютой и отображаем его в селекте -->
                        <?php foreach ($money as $moneyKey => $moneyVal):?>
                        <option>
                            <?= $moneyVal['name']?>
                        </option>
                       <?php endforeach ?>

                    </select>
                    <button type="submit">Выбрать</button>

                </div>
            </form>
            <div class="table" >
                <table class="table table-dark table-striped table-bordered">
                    <thead>
                        <th>Название товара</th>
                        <th>Цена в <?=  $course ?></th>
                        <th>Цена в <?=  $course ?> со скидкой</th>
                    </thead>
                    <!-- Второй массив!!!-->
                    <?php foreach ($goods as $trade): ?>
                    <tr>
                        <td>
                            <?= $trade['title']?>
                        </td>
                        <td>
                            <?= $trade['price_val']?>

                        </td>
                        <td>
                            <?php $getPrice = getPriceWithDiscount($trade['price_val'], $trade['discount_type'], $trade['discount_val'])?>
<!--                            --><?//= convertPrice($getPrice, $course[$_SESSION['viewCur']['course']]); ?>
                        </td>
                    </tr>
                     <?php endforeach ?>
                </table>
            </div>
        </div>
    </div>




<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
